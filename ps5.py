"""
CS6476 Problem Set 5 imports. Only Numpy and cv2 are allowed.
"""
import numpy as np
import cv2


# Assignment code


class KalmanFilter(object):
    """A Kalman filter tracker"""

    def __init__(self, init_x, init_y, Q=0.1 * np.eye(4), R=0.1 * np.eye(2)):
        """Initializes the Kalman Filter

        Args:
            init_x (int or float): Initial x position.
            init_y (int or float): Initial y position.
            Q (numpy.array): Process noise array.
            R (numpy.array): Measurement noise array.
        """

        self.state = np.array([init_x, init_y, 0., 0.])  # state
         #self.x = np.array([[init_x], [init_y], [0.], [0.]])
        #self.x = np.copy(self.state)
        self.P = 1000*np.eye(4)
        self.D = np.array([[1., 0. ,1. ,0.],[0. ,1. ,0. ,1.],[0. ,0. ,1. ,0.],[0. ,0. ,0. ,1.]])
        self.H = np.array([[1., 0., 0., 0.],[0., 1., 0., 0.]])
        self.Q = Q
        self.R = R
        self.I = np.eye(4)
         
         
 
    def predict(self):
        self.state = np.matmul(self.D, self.state)
        p1 = np.matmul(self.D, self.P)
        p1 = np.matmul(p1, np.transpose(self.D))
        self.P = p1 + self.Q
        #self.P = np.matmul((np.matmul(self.D, self.P)),np.transpose(self.D)) + self.Q
         
 
    def correct(self, meas_x, meas_y):
        
#         S = np.matmul((np.matmul(self.H, self.P)),np.transpose(self.H)) + self.R
        k1 = np.matmul(self.P, np.transpose(self.H))
        k2 = np.matmul(self.H, self.P)
        k2 = np.matmul(k2, np.transpose(self.H)) + self.R
        k2 = np.linalg.inv(k2)
        K = np.matmul(k1, k2)
        z = np.array([meas_x, meas_y])
        y = z - np.matmul(self.H, self.state)
#         K = np.matmul((np.matmul(self.P, np.transpose(self.H))), np.linalg.inv(S))
        self.state = self.state + np.matmul(K,y)
        p1 = np.eye(4) - np.matmul(K,self.H)
        self.P = np.matmul(p1, self.P)
        #self.P = np.matmul((self.I - np.matmul(K,self.H)),self.P)
         
         
#         y = np.transpose(z) - np.matmul(self.H, self.x)
#         S = np.matmul((np.matmul(self.H, self.P)),np.transpose(self.H)) + self.R
#         K = np.matmul((np.matmul(self.P, np.transpose(self.H))), np.linalg.inv(S))
#         self.x = self.x + np.matmul(K,y)
#         self.P = np.matmul((self.I - np.matmul(K,self.H)),self.P)
#         
# 
    def process(self, measurement_x, measurement_y):
 
       self.predict()
       self.correct(measurement_x, measurement_y)
# 
       return self.state[0], self.state[1]
# =============================================================================


class ParticleFilter(object):
    """A particle filter tracker.

    Encapsulating state, initialization and update methods. Refer to
    the method run_particle_filter( ) in experiment.py to understand
    how this class and methods work.
    """

    def __init__(self, frame, template, **kwargs):
        """Initializes the particle filter object.

        The main components of your particle filter should at least be:
        - self.particles (numpy.array): Here you will store your particles.
                                        This should be a N x 2 array where
                                        N = self.num_particles. This component
                                        is used by the autograder so make sure
                                        you define it appropriately.
                                        Make sure you use (x, y)
        - self.weights (numpy.array): Array of N weights, one for each
                                      particle.
                                      Hint: initialize them with a uniform
                                      normalized distribution (equal weight for
                                      each one). Required by the autograder.
        - self.template (numpy.array): Cropped section of the first video
                                       frame that will be used as the template
                                       to track.
        - self.frame (numpy.array): Current image frame.

        Args:
            frame (numpy.array): color BGR uint8 image of initial video frame,
                                 values in [0, 255].
            template (numpy.array): color BGR uint8 image of patch to track,
                                    values in [0, 255].
            kwargs: keyword arguments needed by particle filter model:
                    - num_particles (int): number of particles.
                    - sigma_exp (float): sigma value used in the similarity
                                         measure.
                    - sigma_dyn (float): sigma value that can be used when
                                         adding gaussian noise to u and v.
                    - template_rect (dict): Template coordinates with x, y,
                                            width, and height values.
        """
        self.num_particles = kwargs.get('num_particles')  # required by the autograder
        self.sigma_exp = kwargs.get('sigma_exp')  # required by the autograder
        self.sigma_dyn = kwargs.get('sigma_dyn')  # required by the autograder
        self.template_rect = kwargs.get('template_coords')  # required by the autograder
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)
        #print kwargs
        #print self.template_rect
    
        x = kwargs['template_coords']['x']
        y = kwargs['template_coords']['y']
        w = kwargs['template_coords']['w']
        h = kwargs['template_coords']['h']
        p_height = np.random.randint(0, frame.shape[0], size=(self.num_particles, 1))
        p_width = np.random.randint(0, frame.shape[1], size=(self.num_particles, 1))
        
        #print p_height
        #print kwargs
        self.template = template
        self.template_gray = cv2.cvtColor(self.template, cv2.COLOR_BGR2GRAY)
        self.frame = frame
        self.gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #self.particles = np.hstack((p_height, p_width)).astype(np.float_)  # Initialize your particles array. Read the docstring.
        #self.particles = np.array([np.random.uniform(0, np.array(self.gray_frame.shape)[i], self.num_particles)for i in range(1,-1,-1)]).T
        self.particles=np.zeros((self.num_particles,2))
        self.particles[:,0] = np.random.uniform(self.template_rect['x'],self.template_rect['x'] + self.template_rect['w'],self.num_particles)
        self.particles[:,1] = np.random.uniform(self.template_rect['y'],self.template_rect['y'] + self.template_rect['h'], self.num_particles)
        self.weights =  np.ones(self.num_particles) / self.num_particles # Initialize your weights array. Read the docstring.
        # Initialize any other components you may need when designing your filter.
        self.indices = np.arange(self.num_particles)
        

    def get_particles(self):
        """Returns the current particles state.

        This method is used by the autograder. Do not modify this function.

        Returns:
            numpy.array: particles data structure.
        """
        
        return self.particles

    def get_weights(self):
        """Returns the current particle filter's weights.

        This method is used by the autograder. Do not modify this function.

        Returns:
            numpy.array: weights data structure.
        """
        
        
        return self.weights
    



    def get_error_metric(self, template, frame_cutout):
        """Returns the error metric used based on the similarity measure.

        Returns:
            float: similarity value.
        """
        if np.subtract(template.shape, frame_cutout.shape).any():
            return 0
        else: 
            
            error = np.sum(np.subtract(template, frame_cutout, dtype=np.float32)**2)
            error = error/(np.float(template.shape[0] * template.shape[1]))
            weight = np.exp(-error / (2.0 * (self.sigma_exp ** 2)))
            return weight
        
# =============================================================================
#     def get_error_metric(self, template, frame_cutout):
#         """Returns the error metric used based on the similarity measure.
# 
#         Returns:
#             float: similarity value.
#         """
#         if np.subtract(template.shape, frame_cutout.shape).any():
#             # print("I am 0")
#             return 0
#         else:    
#             # print("I am 1")
#             mse = np.sum(np.subtract(template, frame_cutout, dtype=np.float32) ** 2)
#             mse = mse/float(template.shape[0] * template.shape[1])
#             return np.exp(-mse/ 2 / self.sigma_exp ** 2)
# =============================================================================
        
    def resample_particles(self):
        """Returns a new set of particles
        
        This method does not alter self.particles.

        Use self.num_particles and self.weights to return an array of
        resampled particles based on their weights.

        See np.random.choice or np.random.multinomial.
        
        Returns:
            numpy.array: particles data structure.
        """
        
        #new_weight = np.array([])
        
        #for i in range(self.num_particles):
        #print np.random.choice(self.num_particles, 1, p=self.weights)
        num_par_array = np.arange(self.num_particles)
        j = np.random.choice(num_par_array, self.num_particles, 1, p=self.weights)
        resampled = self.particles[j]
            
            
        return resampled

    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        Implement the particle filter in this method returning None
        (do not include a return call). This function should update the
        particles and weights data structures.

        Make sure your particle filter is able to cover the entire area of the
        image. This means you should address particles that are close to the
        image borders.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """
        
        x = self.template_rect['x']
        y = self.template_rect['y']
        w = self.template_rect['w']
        h = self.template_rect['h']
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #new_particles = np.zeros((0, 2))
        new_weights = np.array([])
        new_particles = self.resample_particles()
        #print new_particles
        new_particles = new_particles + np.random.normal(0, self.sigma_dyn, new_particles.shape)
        #print new_particles
        new_particles[:, 0][new_particles[:, 0] < w/2] = w/2
        new_particles[:, 1][new_particles[:, 1] < h/2] = h/2
        #new_particles[:,0] = np.clip(new_particles[:,0], 0, frame.shape[0]-1)
        #print new_particles_x
        #new_particles[:,1] = np.clip(new_particles[:,1], 0, frame.shape[1]-1)
        #print new_particles_x
        new_particles[:, 0][new_particles[:, 0] > (self.gray_frame.shape[1] - w/2)] = self.gray_frame.shape[1] - w/2
        new_particles[:, 1][new_particles[:, 1] > (self.gray_frame.shape[0] - h/2)] = self.gray_frame.shape[0] - h/2
        #new_particles[:,0] = np.clip(new_particles[:,0], 0, frame.shape[0]-1)
        #print new_particles_x
        #new_particles[:,1] = np.clip(new_particles[:,1], 0, frame.shape[1]-1)
        #print new_particles_x
        #new_particles[:,0] = np.clip(new_particles[:,0], 0, frame.shape[0]-1)
        #print new_particles_x
        #new_particles[:,1] = np.clip(new_particles[:,1], 0, frame.shape[1]-1)
        #print new_particles_x
        for i in range(self.num_particles):
            extreme_left = (new_particles[i,0]- w/2).astype(np.int)
            extreme_top = (new_particles[i,1] - h/2).astype(np.int)
            cut_out = frame_gray[extreme_top:extreme_top+h, extreme_left:extreme_left+w]
            
            new_weight = self.get_error_metric(self.template_gray, cut_out)
            
            new_weights = np.append(new_weights, new_weight)
            
        self.particles = new_particles
        #print new_weights
        self.weights = new_weights/np.sum(new_weights)
        
        

    def render(self, frame_in):
        """Visualizes current particle filter state.

        This method may not be called for all frames, so don't do any model
        updates here!

        These steps will calculate the weighted mean. The resulting values
        should represent the tracking window center point.

        In order to visualize the tracker's behavior you will need to overlay
        each successive frame with the following elements:

        - Every particle's (x, y) location in the distribution should be
          plotted by drawing a colored dot point on the image. Remember that
          this should be the center of the window, not the corner.
        - Draw the rectangle of the tracking window associated with the
          Bayesian estimate for the current location which is simply the
          weighted mean of the (x, y) of the particles.
        - Finally we need to get some sense of the standard deviation or
          spread of the distribution. First, find the distance of every
          particle to the weighted mean. Next, take the weighted sum of these
          distances and plot a circle centered at the weighted mean with this
          radius.

        This function should work for all particle filters in this problem set.

        Args:
            frame_in (numpy.array): copy of frame to render the state of the
                                    particle filter.
        """

        x_weighted_mean = 0
        y_weighted_mean = 0
        w = self.template_rect['w']
        h = self.template_rect['h']
        
        for particle in self.particles:
            cv2.circle(frame_in, (int(particle[0]),int(particle[1])), 1, (0, 255, 255), lineType=cv2.LINE_AA  )
        for i in range(self.num_particles):
            x_weighted_mean += self.particles[i, 0] * self.weights[i]
            y_weighted_mean += self.particles[i, 1] * self.weights[i]
        
        
        #print 'x_weighted_mean'
        #print x_weighted_mean
        x = x_weighted_mean - w/2
        y = y_weighted_mean - h/2
#        if np.isnan(x):
#            x = 0
#        if np.isnan(y):
#            y = 0
        cv2.rectangle(frame_in, (int(x), int(y)), (int(x)+w, int(y)+h), (0, 255, 255), lineType=cv2.LINE_AA) 
        particles2 = self.particles.copy()
        particles2[:,:1] = (particles2[:,:1] - x_weighted_mean) ** 2
        particles2[:,1:] = (particles2[:,1:] - y_weighted_mean) ** 2
        distance = np.sqrt(np.sum(particles2, axis=1))
        
        radius = np.average(distance, axis=0, weights=self.weights)
#        if np.isnan(x_weighted_mean):
#            x_weighted_mean = 0
#        if np.isnan(y_weighted_mean):
#            y_weighted_mean = 0
#        if np.isnan(radius):
#            radius = 0
        cv2.circle(frame_in, (int(x_weighted_mean), int(y_weighted_mean)), int(radius), (0, 0, 255), lineType=cv2.LINE_AA)
        # Complete the rest of the code as instructed.
        


class AppearanceModelPF(ParticleFilter):
    """A variation of particle filter tracker."""

    def __init__(self, frame, template, **kwargs):
        """Initializes the appearance model particle filter.

        The documentation for this class is the same as the ParticleFilter
        above. There is one element that is added called alpha which is
        explained in the problem set documentation. By calling super(...) all
        the elements used in ParticleFilter will be inherited so you do not
        have to declare them again.
        """

        super(AppearanceModelPF, self).__init__(frame, template, **kwargs)  # call base class constructor

        self.alpha = kwargs.get('alpha')  # required by the autograder
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value) 
        #self.template = template
    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "Appearance Model" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame, values in [0, 255].

        Returns:
            None.
        """
        
        x = self.template_rect['x']
        y = self.template_rect['y']
        w = self.template_rect['w']
        h = self.template_rect['h']
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #new_particles = np.zeros((0, 2))
        new_weights = np.array([])
        new_particles = self.resample_particles()
        #print new_particles
        new_particles = new_particles + np.random.normal(0, self.sigma_dyn, new_particles.shape)
        #print new_particles
        
        
        
        new_particles[:, 0][new_particles[:, 0] < w/2] = w/2
        new_particles[:, 1][new_particles[:, 1] < h/2] = h/2
        #new_particles[:,0] = np.clip(new_particles[:,0], 0, frame.shape[0]-1)
        #print new_particles_x
        #new_particles[:,1] = np.clip(new_particles[:,1], 0, frame.shape[1]-1)
        #print new_particles_x
        new_particles[:, 0][new_particles[:, 0] > (self.gray_frame.shape[1] - w/2)] = self.gray_frame.shape[1] - w/2
        new_particles[:, 1][new_particles[:, 1] > (self.gray_frame.shape[0] - h/2)] = self.gray_frame.shape[0] - h/2
        #new_particles[:,0] = np.clip(new_particles[:,0], 0, frame.shape[0]-1)
        #print new_particles_x
        #new_particles[:,1] = np.clip(new_particles[:,1], 0, frame.shape[1]-1)
        #print new_particles_x
        #new_particles[:,0] = np.clip(new_particles[:,0], 0, frame.shape[0]-1)
        #print new_particles_x
        #print new_particles_x
        #print new_particles
        for i in range(self.num_particles):
            extreme_left = (new_particles[i,0]- w/2).astype(np.int)
            extreme_top = (new_particles[i,1] - h/2).astype(np.int)
            cut_out = frame_gray[extreme_top:extreme_top+h, extreme_left:extreme_left+w]
#            print cut_out.shape
#            print self.template_gray.shape
#            cv2.imshow('cut_out',cut_out)
#            cv2.waitKey(0)
            new_weight = self.get_error_metric(self.template_gray, cut_out)
            #print new_weight
            new_weights = np.append(new_weights, new_weight)
        
        #print new_weights
        self.particles = new_particles
        #print new_weights
        self.weights = new_weights/np.sum(new_weights)
        
        best_weights = np.copy(self.weights)
        best_weights_indices = np.argsort(best_weights)
        best_particles = new_particles[best_weights_indices]
        best_weights = best_weights[best_weights_indices]
        
#        for particle in self.particles:
#            cv2.circle(frame, (int(best_particles[0]),int(best_particles[1])), 1, (0, 255, 255), lineType=cv2.CV_AA )
        weighted_mean_best_x = 0
        weighted_mean_best_y = 0
        for i in range(self.num_particles):
            weighted_mean_best_x += best_particles[i, 0] * best_weights[i]
            weighted_mean_best_y += best_particles[i, 1] * best_weights[i]
          
        weighted_mean_best_x = weighted_mean_best_x/sum(best_weights)
        weighted_mean_best_y = weighted_mean_best_y/sum(best_weights)
        best_x = int(weighted_mean_best_x - w/2)
        best_y = int(weighted_mean_best_y - h/2)  
        
        best_cut_out = frame_gray[best_y:best_y+h, best_x:best_x+w]
        self.template_gray = self.alpha * best_cut_out + (1-self.alpha) * self.template_gray
        


class MDParticleFilter(AppearanceModelPF):
    """A variation of particle filter tracker that incorporates more dynamics."""

    def __init__(self, frame, template, **kwargs):
        """Initializes MD particle filter object.

        The documentation for this class is the same as the ParticleFilter
        above. By calling super(...) all the elements used in ParticleFilter
        will be inherited so you don't have to declare them again.
        """

        super(MDParticleFilter, self).__init__(frame, template, **kwargs)  # call base class constructor
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)
        self.frame_count = 0
        self.temprory_particles = 0
        self.mean_error = 0
        self.temp_weights = 0
        self.occlusion = 0
        self.rms_previous = 0
        
#        self.occlusion_flag1 = 0
#        self.occlusion_flag2 = 0
        
    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "More Dynamics" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """
        self.frame_count += 1
        alpha = 0.10
        # shrink the the template after every 5 frames
#        if ((self.frame_count % 6) == 0):
#            # print(self.frame_count)
#            self.template_gray = cv2.resize(self.template_gray,
#                                (int(0.99 * self.template_rect['w']), int(0.99 * self.template_rect['h'])), 
#                                interpolation = cv2.INTER_AREA)            
            
        self.template_rect['w'] = self.template_gray.shape[1]
        self.template_rect['h'] = self.template_gray.shape[0]

        # hold old values of particles and weights
#        old_particles = np.copy(self.particles)
#        old_weights = np.copy(self.weights)

        w = self.template_rect['w']
        h = self.template_rect['h']
        

        
        
        
        new_particles = self.resample_particles()
   
        
        new_particles = new_particles + np.random.normal(0, self.sigma_dyn, new_particles.shape)

        

        new_particles[:, 0][new_particles[:, 0] < w/2] = w/2
        new_particles[:, 1][new_particles[:, 1] < h/2] = h/2
        #new_particles[:,0] = np.clip(new_particles[:,0], 0, frame.shape[0]-1)
        #print new_particles_x
        #new_particles[:,1] = np.clip(new_particles[:,1], 0, frame.shape[1]-1)
        #print new_particles_x
        new_particles[:, 0][new_particles[:, 0] > (self.gray_frame.shape[1] - w/2)] = self.gray_frame.shape[1] - w/2
        new_particles[:, 1][new_particles[:, 1] > (self.gray_frame.shape[0] - h/2)] = self.gray_frame.shape[0] - h/2

                
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        new_weights = ([])
        for i in range(self.num_particles):
            extreme_left = (new_particles[i,0]- w/2).astype(np.int)
            extreme_top = (new_particles[i,1] - h/2).astype(np.int)
            cut_out = frame_gray[extreme_top:extreme_top+h, extreme_left:extreme_left+w]
            
            new_weight = self.get_error_metric(self.template_gray, cut_out)
            
            new_weights = np.append(new_weights, new_weight)

        new_weights = new_weights/ np.sum(new_weights)        

        # update template
        best_weights = np.copy(new_weights)
        best_weights_inds =  np.argsort(best_weights)        
        best_weights = best_weights[best_weights_inds]
        best_particles = np.copy(new_particles)
        best_particles = best_particles[best_weights_inds]

        weighted_mean_best_x = 0
        weighted_mean_best_y = 0        
        
        for i in range(best_particles.shape[0]):
            weighted_mean_best_x += best_particles[i, 0] * best_weights[i]
            weighted_mean_best_y += best_particles[i, 1] * best_weights[i]        

        weighted_mean_best_x =  weighted_mean_best_x/sum(best_weights)
        weighted_mean_best_y =  weighted_mean_best_y/sum(best_weights)
        
        #print ('hi')
        #print (best_x)
        best_x = (weighted_mean_best_x - w/2).astype(np.int)
        best_y = (weighted_mean_best_y - h/2).astype(np.int)        
        
        beta = frame_gray[best_y:best_y + h, best_x:best_x + w]

        # mse = ((best_template - self.template_gray) ** 2).mean()        
    
        self.occlusion = 0                
        
        
        distance = np.sqrt(np.sum((new_particles - (weighted_mean_best_x, weighted_mean_best_y))**2,1))
        radius = np.sum(np.multiply(distance, new_weights))
#         self.occlusion = 0
#         if self.frame_count > 5:
#             if radius > 10:
#                 print 'occlusion'
#                 self.occlusion = 1
#             else:
#                 self.occlusion = 0
# #        rms = np.sqrt(np.sum((beta - self.template_gray) ** 2))
# #        
# #        if self.frame_count > 2:
# #            change = 100* (rms - self.rms_previous)/self.rms_previous
# #            print change
#         #if (self.frame_count)%5 == 0:
#         #print(change)
# 
#         #occlusion = 0
#         
# #        if self.frame_count > 10:
# #            if change > 20.00:
# #                if self.occlusion == 0:
# #                    self.occlusion = 1
# #                    
# #                    print 'occlusion'
# #                else:
# #                    self.occlusion = 0
# #                    print 'occlusion removed'            
        #print(radius)
        if radius > 5:
            self.occlusion = 1

#         radius = np.average(distance, axis=0, weights=self.weights)
#         print radius
#         #rms = np.sqrt(np.sum((template_temp - self.template_gray) ** 2))
#         
#         if self.frame_count > 2:
#             change = 100* (rms - self.rms_previous)/self.rms_previous
#             print change
#         #if (self.frame_count)%5 == 0:
#         #print(change)
# 
#         #occlusion = 0
#         
#         if self.frame_count > 10:
#             if change > 45.00:
#                 if self.occlusion == 0:
#                     self.occlusion = 1
#                     
#                     print 'occlusion'
#                 else:
#                     self.occlusion = 0
#                     print 'occlusion removed'
#         
# # =============================================================================
# #             if (self.frame_count)%20 == 0:
# #                 print ('mean =' + str(np.sum(new_weights)))
# #                 print np.subtract(2.00 ** -10, np.mean(new_weights))
# #                 if np.subtract(2.00 ** -15, np.mean(new_weights)) < 0:
# #                     print np.subtract(np.mean(new_weights), 2.00 ** -10)
# #                     self.occlusion = 1
# #                     
# #                     print 'occluded'
# #                 else:
# #                     self.occlusion = 0
# # =============================================================================
#              
                
        if self.occlusion == 0:        
            self.template_gray = alpha * beta + (1 - alpha) * self.template_gray
            # update particles
            self.particles = new_particles
            # update weights
            self.weights = new_weights
        if ((self.frame_count % 5) == 0):
            # print(self.frame_count)
            self.template_gray = cv2.resize(self.template_gray,
                                (int(0.99 * self.template_rect['w']), int(0.99 * self.template_rect['h'])), 
                                interpolation = cv2.INTER_AREA)    

        
            