# Object Tracking

In this project, tracking methods for image sequences and videos was explored. The main algorithms used were Kalman and Particle Filters. We designed and implemented a Kalman and a Particle filters from the ground up.

The Kalman Filter (KF) is a method used to track linear dynamical systems with gaussian noise. Both the predicted and the corrected states are gaussians, allowing us to keep the mean and covariance of the state in order to run this filter. We used the kalman filter on the provided images to track a moving circle.

For Particle Filter, we tracked an image patch template taken from the first frame of the video. The model was simply the image patch, and the state was only the 2D center location of the patch. Thus each particle would be (u, v) pixel location (where u and v are the row and column number respectively) representing a proposed location for the center of the template window. 

I used a basic function to estimate the dissimilarity between the image patch and a window of equal size in the current image, the Mean Squared Error.

In order to visualize the tracker’s behavior I overlayed each successive frame with the following elements:

	1) Every particle’s (x,y) location in the distribution was plotted by drawing a colored dot point on the image. This was the center of the window, not the corner.

	2) I drew the rectangle of the tracking window associated with the Bayesian estimate for the current location which is simply the weighted mean of the (x,y) of the particles.
	
	3) Finally I need to get some sense of the standard deviation or spread of the distribution. 
	
		- First, I found the distance of every particle to the weighted mean. 
		
		- Next, I took the weighted sum of these distances and plotted a circle centered at the weighted mean with this radius.
		
The tracker developed in this project also worked in case of occlusions as well.

## Instructions to run the code
	1) run experiment.py
	
## Outputs:
## Face tracking using Particle Filters
	
![face1](output/part3a.gif)

## Hand tracking using particle filters
![hand](output/part2b.gif)

## Tracking incase of Occlusions
![occul](output/part4.gif)






